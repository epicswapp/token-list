# epic-token-list

This repo hosts the token list for the https://epicsw.app interface.

To list your coin on the default HyperJump list open a pull request with your logo image and token information (added to /images & /tokens.json respectively)

